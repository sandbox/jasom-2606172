--------------------------------------------------------------------------------
                             Upload progress fix
--------------------------------------------------------------------------------

Maintainers:
------------
* Jasom Dotnet (https://www.drupal.org/user/290309)      

Project homepage
------------
* https://www.drupal.org/sandbox/jasom/2606172

Upload progress fix for anonymous users on sites with enabled cache. See 
original patch by Jamix https://www.drupal.org/node/1627162 

Installation
------------
* Download using "git clone..."
* Enable as usual
* Clear all caches
 
Credit
------
* Jasom Dotnet (https://www.drupal.org/user/290309)
* Art Matsak - Jamix (https://www.drupal.org/user/244805) 
* Tomas Teicher (https://www.drupal.org/user/668042)
